<?php
use Illuminate\Http\Request;
use Database\Constants;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TokenController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



class RouteConstants{
    public const APP_REGISTER = "oauth/register";
    public const APP_TOKEN = "oauth/token";
    public const APP_TOKEN_REFRESH = "oauth/token_refresh";
    public const APP_REGISTER_HANDLER = "RegistrationController@store";
    public const APP_TOKEN_HANDLER = "TokenController@handleAccessTokenRequest";
    public const APP_REFRESH_TOKEN_HANDLER = "TokenController@handleRefreshTokenRequest";
}



Route::post(RouteConstants::APP_REGISTER, RouteConstants::APP_REGISTER_HANDLER);
Route::post(RouteConstants::APP_TOKEN, RouteConstants::APP_TOKEN_HANDLER);
Route::post(RouteConstants::APP_TOKEN_REFRESH, RouteConstants::APP_REFRESH_TOKEN_HANDLER);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});