<?php

use App\Utils\Helper;
use Illuminate\Http\Requestt;
use Illuminate\Http\RedirectResponse;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
class WebRoute{
    public const PATH_ROOT = "/";
    public const PATH_LOGIN = "/login";
    public const PATH_LOGOUT = "/logout";
    public const PATH_LOGIN_POST = "/loginpost";
    public const PATH_OAUTH_DIALOG = "/oauth_dialog";
    public const PATH_MAIN_PAGE = "/mainpage";
    public const PATH_ALLOWED = "/allowed";
    public const PATH_REJECTED = "/rejected";
    
    public const APP_AUTHORIZE = "oauth/authorize";
    public const VIEW_WELCOME = 'welcome';
    public const VIEW_LOGIN = 'login';
    public const VIEW_OAUTH_DIALOG = "oauth_confirm";
    public const VIEW_MAIN_PAGE = "mainpage";
    public const APP_AUTH_HANDLER = "AuthController@handleRequest";
    public const APP_LOGIN_HANDLER = "LoginController@handleLoginPostRequest";
    public const APP_GET_LOGIN_HANDLER = "LoginController@handleLoginGetRequest";
    public const APP_LOGOUT_HANDLER = "LoginController@handleLogoutRequest";
    public const MAIN_PAGE_HANDLER = "LoginController@handleMainPageRequest";
    public const OAUTH_DIALOG_HANDLER = "AuthController@handleOauthDialogRequest";
    public const ALLOWED_OAUTH_HANDLER = "AuthController@handleApproveAuth";
    public const REJECT_OAUTH_HANDLER = "AuthController@handleDisapproveAuth";

    public const PARAM_APP_NAME = "app_name";
    public const PARAM_APP_ID = "app_id";
    public const PARAM_USER = "user";
    public const PARAM_EMAIL_ID = "emailId";
}
Route::get(WebRoute::PATH_ROOT, function () {
    return view(WebRoute::VIEW_WELCOME);
});


Route::get(WebRoute::PATH_LOGIN, WebRoute::APP_GET_LOGIN_HANDLER);
Route::get(WebRoute::PATH_LOGOUT, WebRoute::APP_LOGOUT_HANDLER);
Route::post(WebRoute::PATH_LOGIN, WebRoute::APP_LOGIN_HANDLER);
Route::get(WebRoute::APP_AUTHORIZE, WebRoute::APP_AUTH_HANDLER);
Route::get(WebRoute::PATH_OAUTH_DIALOG, WebRoute::OAUTH_DIALOG_HANDLER);
Route::get(WebRoute::PATH_MAIN_PAGE, WebRoute::MAIN_PAGE_HANDLER);
Route::get(WebRoute::PATH_ALLOWED, WebRoute::ALLOWED_OAUTH_HANDLER);
Route::get(WebRoute::PATH_REJECTED, WebRoute::REJECT_OAUTH_HANDLER);