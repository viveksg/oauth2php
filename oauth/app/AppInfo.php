<?php

namespace App;

include_once("../database/constants/dbconstants.php");

use Illuminate\Database\Eloquent\Model;
use Database\Constants;

class AppInfo extends Model
{
    protected $fillable = [
        Constants::FIELD_APP_ID,
        Constants::FIELD_APP_NAME,
        Constants::FIELD_CLIENT_SECRET,
        Constants::FIELD_REDIRECT_URI
    ];
}
