<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Database\Constants;

class CredentialInfo extends Model
{
    protected $fillable = [
        Constants::FIELD_APP_ID,
        Constants::FIELD_AUTH_CODE,
        Constants::FIELD_ACCESS_TOKEN,
        Constants::FIELD_REFRESH_TOKEN,
        Constants::FIELD_TOKEN_VALIDITY,
        Constants::FIELD_REFRESH_TOKEN_VALIDITY
    ];
}
