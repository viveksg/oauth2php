<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Database\Constants;
use App\AppInfo;
use App\Utils\Helper;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public const REQ_PARAM_APP_NAME = "app_name";
    public const REQ_PARAM_REDIRECT_URL = "redirect_uri";
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $json_response = [];
        $data = [];
        $app_name = $request->input(RegistrationController::REQ_PARAM_APP_NAME);
        $redirect_uri = $request->input(RegistrationController::REQ_PARAM_REDIRECT_URL);
        if(!($this->doesAppAlreadyExists($app_name))){
             $app_id = Helper::generateRandomId(Constants::APP_INFO_TABLE, Constants::FIELD_APP_ID);
             $app_secret = Helper::generateRandomId(Constants::APP_INFO_TABLE, Constants::FIELD_CLIENT_SECRET);
             AppInfo::create([
                 Constants::FIELD_APP_ID => $app_id, 
                 Constants::FIELD_APP_NAME => $app_name, 
                 Constants::FIELD_CLIENT_SECRET => $app_secret, 
                 Constants::FIELD_REDIRECT_URI => $redirect_uri]);
             $data[Helper::STATUS] = Helper::HTTP_STATUS_OK;
             $data[Helper::APP_ID] = $app_id;
             $data[Helper::APP_SECRET] = $app_secret;
             $json_response[Helper::DATA] = $data;
             return response()->json($json_response, Helper::HTTP_STATUS_OK);
        }
        $json_response[Helper::MSG] = Helper::MSG_APP_EXISTS;
        return response()->json($json_response, Helper::HTTP_STATUS_BAD_REQUEST);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   
    private function doesAppAlreadyExists($app_name){
        return Helper::checkIfRecordExists(Constants::APP_INFO_TABLE, Constants::FIELD_APP_NAME, $app_name);
    }
}
