<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\AppInfo;
use App\Utils\Helper;
use Database\Constants;

class AuthController extends Controller
{
    public function handleRequest(Request $request)
    {   
        $app_id = $request->query(Constants::FIELD_APP_ID);
        $app_secret = $request->input(Constants::FIELD_CLIENT_SECRET);
        if(Helper::checkAppValidity($app_id, $app_secret)){
            Helper::createOauthSession($request, $app_id);
            return redirect(Helper::ROUTE_LOGIN);
        }
        $json_response = [
            Helper::MSG => Helper::MSG_INVALID_APP_CREDENTIALS
        ];
        return response()->json($json_response, Helper::HTTP_STATUS_BAD_REQUEST);
        
    }
    
    public function handleOauthCodeSession(Request $request){
    }

    private function checkAppId($app_id, $app_secret){
        return Helper::checkIfRecordExists(Constants::APP_INFO_TABLE, Constants::FIELD_APP_ID, $app_name);
    }

    public function handleOauthDialogRequest(Request $request){
        $app_id = $request->session()->get(Helper::SESSION_KEY_APPID)[0];
        $data = [
            Helper::APP_ID => $app_id
        ];
        return view(Helper::VIEW_OAUTH_DIALOG, $data);
    }

    public function handleApproveAuth(Request $request){
        $session_id = $request->session()->getId();
        $appId = $request->session()->get(Helper::SESSION_KEY_APPID)[0];
        $auth_code = Helper::generateRandomIdFromSize(Constants::AUTH_SESSION_TABLE, Constants::FIELD_AUTH_CODE,Helper::CODE_SIZE);
        $redirect_uri = Helper::getDBValue(Constants::APP_INFO_TABLE,Constants::FIELD_APP_ID,$appId,Constants::FIELD_REDIRECT_URI);
        $complete_uri = $redirect_uri."?".Helper::AUTH_CODE."=".$auth_code;
        Helper::saveAuthSession($session_id, $appId, $auth_code, Helper::AUTH_CODE_ACTIVE);
        Helper::cleanOauthSession($request);
        return redirect($complete_uri);
    }

    public function handleDisapproveAuth(Request $request){
        Helper::cleanOauthSession($request);
        $request->session()->save();
        return redirect(Helper::ROUTE_LOGIN);
    }
   
}
