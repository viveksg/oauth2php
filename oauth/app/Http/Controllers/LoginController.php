<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Utils\Helper;
use Database\Constants;

class LoginController extends Controller
{
    
    public function handleLoginGetRequest(Request $request){
        if(Helper::isUserAlreadyLoggedIn($request)){
            if(Helper::isOauthAuthorizationInProgress($request))
                 return view(Helper::VIEW_OAUTH_DIALOG);      
            return redirect(Helper::ROUTE_MAIN_PAGE);   
        }
        return view(Helper::VIEW_LOGIN);
    }
    public function handleLoginPostRequest(Request $request){
        $email = $request->input(Helper::PARAM_EMAIL);
        $password = $request->input(Helper::PARAM_PASSWORD);
        if(Helper::isUserCredentialsValid($email, $password)){
            Helper::createLoginSession($request, $email);
            if(Helper::isOauthAuthorizationInProgress($request))
                return redirect(Helper::ROUTE_OAUTH_DIALOG);
            else
                return redirect(Helper::ROUTE_MAIN_PAGE);    
        }
        return redirect(Helper::ROUTE_LOGIN);
    }
    
    public function handleLogoutRequest(Request $request){
        Helper::cleanLoginSession($request);
        return redirect(Helper::ROUTE_LOGIN);
    }

    public function handleMainPageRequest(Request $request){
        $email_id = $request->session()->get(Helper::SESSION_KEY_LOGGED_IN_EMAIL);
        $data = [
            Helper::PARAM_EMAIL =>   $email_id[0]
        ];
        return view(Helper::VIEW_MAIN_PAGE, $data);
    }
}
