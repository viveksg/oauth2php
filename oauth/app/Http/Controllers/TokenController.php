<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppInfo;
use App\Auth_Session;
use App\CredentialInfo;
use App\Utils\Helper;
use Database\Constants;

class TokenController extends Controller
{
    public static function handleAccessTokenRequest(Request $request)
    {  
        $appId = $request->input(Helper::APP_ID);
        $code = $request->input(Helper::AUTH_CODE);
        if(Helper::verifyCodeData($appId, $code)){
            $token = Helper::generateRandomIdFromSize(Constants::CRED_INFO_TABLE,
                                                      Constants::FIELD_ACCESS_TOKEN,
                                                      Helper::TOKEN_SIZE);

            $refresh_token = Helper::generateRandomIdFromSize(Constants::CRED_INFO_TABLE,
                                                              Constants::FIELD_REFRESH_TOKEN,
                                                              Helper::REFRESH_TOKEN_SIZE);
            $token_validity = Helper::TOKEN_VALIDITY;
            $refresh_token_validity = Helper::REFRESH_TOKEN_VALIDITY;                                                  
            Helper::insertNewTokenRecord($appId, $code, $token, $refresh_token, $token_validity, $refresh_token_validity);
            Helper::invalidateAuthSession($appId, $code);
            $data = [
                 Helper::ACCESS_TOKEN => $token,
                 Helper::REFRESH_TOKEN => $refresh_token,
                 Constants::FIELD_TOKEN_VALIDITY => Helper::TOKEN_VALIDITY,
                 Constants::FIELD_REFRESH_TOKEN_VALIDITY => Helper::REFRESH_TOKEN_VALIDITY
            ];
            return response()->json($data, Helper::HTTP_STATUS_OK);
        }
        $data = [
            Helper::MSG => Helper::MSG_INVALID_TOKEN_CREDENTIALS
        ];
        return response()->json($data, Helper::HTTP_STATUS_BAD_REQUEST);
    }

    public static function handleRefreshTokenRequest(Request $request){
        $appId = $request->input(Helper::APP_ID);
        $refreshToken = $request->input(Helper::REFRESH_TOKEN);
        $appSecret = $request->input(Helper::APP_SECRET);
        echo "Test 1: ".(Helper::checkAppValidity($appId, $appSecret))."\n";
        echo "Test 2: ". (Helper::verifyTokenData($appId, Constants::FIELD_REFRESH_TOKEN,$refreshToken,
                                     Constants::FIELD_CREATED_AT, Helper::REFRESH_TOKEN_VALIDITY))."\n";
        if(Helper::checkAppValidity($appId, $appSecret)
           && Helper::verifyTokenData($appId, Constants::FIELD_REFRESH_TOKEN,$refreshToken,
                                     Constants::FIELD_CREATED_AT, Helper::REFRESH_TOKEN_VALIDITY)){
                                        $token = Helper::generateRandomIdFromSize(Constants::CRED_INFO_TABLE,
                                        Constants::FIELD_ACCESS_TOKEN,
                                        Helper::TOKEN_SIZE);
                                        Helper::refreshExistingTokenRecord($appId, $refreshToken, $token);
                                       
                                        $data = [
                                                  Helper::ACCESS_TOKEN => $token,
                                                  Helper::REFRESH_TOKEN => $refreshToken,
                                                  Constants::FIELD_TOKEN_VALIDITY => Helper::TOKEN_VALIDITY,
                                                  Constants::FIELD_REFRESH_TOKEN_VALIDITY => Helper::REFRESH_TOKEN_VALIDITY
                                                ];
                                                return response()->json($data, Helper::HTTP_STATUS_OK);
                                    
                                     }
        
        $data = [
                   Helper::MSG => Helper::MSG_INVALID_REFRESH_TOKEN_CREDENTIALS
                ];
        throw Exception;        
        return response()->json($data, Helper::HTTP_STATUS_BAD_REQUEST);                             

    }

}
