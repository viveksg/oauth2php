<?php
namespace App\Utils;
use App\Auth_Session;
use App\CredentialInfo;
use Database\Constants;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class Helper{
    public const APP_NAME = "app_name";
    public const APP_ID = "app_id";
    public const APP_SECRET = "app_secret";
    public const ACCESS_TOKEN = "access_token";
    public const REFRESH_TOKEN = "refresh_token";
    public const AUTH_CODE = "auth_code";
    public const VALIDITY = "validity";
    public const RESPONSE = "response";
    public const DATA = "data";
    public const MSG = "msg";
    public const STATUS = "status";
    public const SIZE_DEFAULT = 32;
    public const CODE_SIZE = 48;
    public const TOKEN_SIZE = 128;
    public const REFRESH_TOKEN_SIZE = 64;
    public const TOKEN_VALIDITY = 3600;
    public const REFRESH_TOKEN_VALIDITY = 3600 * 24 *15;
    public const KEY_COST = "cost";
    public const BCRYPT_ROUNDS = 12;

    public const SESSION_KEY_OAUTH = "sessoauthkey.oauth_session";
    public const SESSION_KEY_LOGGEDIN = "sessloginkey.logged_in";
    public const SESSION_KEY_APPID = "sessoauthkey.appId";
    public const SESSION_KEY_LOGGED_IN_EMAIL = "sessloginkey.emailId";
    public const SESSION_VAL_OAUTH_ACTIVE = '3000';
    public const SESSION_VAL_LOGGED_IN = '3001';
    public const KEY_GENERATE_TOKEN = "generate_token";
    public const TOKEN_STATUS_PENDING = '1000';
    public const TOKEN_STATUS_GENERATED = '2000';
    public const AUTH_CODE_ACTIVE = '4000';
    public const AUTH_CODE_INACTIVE = '4001';
    public const ROUTE_LOGIN = "/login";
    public const ROUTE_OAUTH_DIALOG = "/oauth_dialog";
    public const ROUTE_MAIN_PAGE = "/mainpage";
    public const HTTP_STATUS_OK = 200;
    public const HTTP_STATUS_BAD_REQUEST = 400;
    public const HTTP_REDIRECT = 302;
    
    public const STATUS_APP_VALID = 255;
    public const STATUS_APP_INVALID = 256;
    public const STATUS_SUCCESS = "success";
    public const STATUS_FAILED = "failed";

    public const MSG_APP_EXISTS = "error: app_name already exists";
    public const MSG_INVALID_APP_CREDENTIALS = "error: invalid app_id or app_secret";
    public const MSG_INVALID_TOKEN_CREDENTIALS = "error: invalid app_id or auth_code";
    public const MSG_INVALID_REFRESH_TOKEN_CREDENTIALS = "error: invalid app_id or refresh_token";
    public const MSG_INVALID_CODE = "error: invalid code";
    public const MSG_TOKEN_EXPIRED = "error: token expired";

    public const PARAM_EMAIL = "emailId";
    public const PARAM_PASSWORD = "password";

    public const VIEW_WELCOME = 'welcome';
    public const VIEW_LOGIN = 'login';
    public const VIEW_OAUTH_DIALOG = "oauth_confirm";
    public const VIEW_MAIN_PAGE = "mainpage";
    public const TIME_FILTER_PART_ONE = "unix_timestamp() - unix_timestamp(";
    public const TIME_FILTER_PART_TWO = ")";


    public static function isValid(){

    }

    public static function generateRandNum(){
       return bin2hex(random_bytes(32));
    }

    public static function generateRandNumFromSize($size){
        return bin2hex(random_bytes($size));
     }
    
    public static function checkIfRecordExists($table, $field, $str_val){
        return DB::table($table)
               ->where($field, 'like','%'.$str_val.'%')
               ->exists();
    }

    public static function generateRandomId($table,$field){
        $randomIdNotGenerated= true;
        $randomId = "";
        while($randomIdNotGenerated){
            $randomId = Helper::generateRandNum();
            if(!(Helper::checkIfRecordExists($table, $field, $randomId)))
               break;
        }
        return $randomId;
    }

    public static function generateRandomIdFromSize($table,$field, $size){
        $randomIdNotGenerated= true;
        $randomId = "";
        while($randomIdNotGenerated){
            $randomId = Helper::generateRandNumFromSize($size);
            if(!(Helper::checkIfRecordExists($table, $field, $randomId)))
               break;
        }
        return $randomId;
    }

    public static function checkAppValidity($app_id, $app_secret){
        return DB::table(Constants::APP_INFO_TABLE)
               ->where(Constants::FIELD_APP_ID, $app_id)
               ->where(Constants::FIELD_CLIENT_SECRET, $app_secret)
               ->exists();
    }

    public static function isUserCredentialsValid($email, $pass){
        $dbpass = DB::table(Constants::USER_TABLE)
                  ->where(Constants::FIELD_USER_EMAIL, $email)
                  ->value(Constants::FIELD_USER_PASSWORD);
        return Helper::verifyPass($pass, $dbpass);              
    }
    
    public static function createLoginSession(Request $request, $email){
        $request->session()->put(Helper::SESSION_KEY_LOGGEDIN, Helper::SESSION_VAL_LOGGED_IN);
        $request->session()->push(Helper::SESSION_KEY_LOGGED_IN_EMAIL, $email);
        $request->session()->save();  
    }
    
    public static function createOauthSession(Request $request, $app_id){
        $request->session()->put(Helper::SESSION_KEY_OAUTH, Helper::SESSION_VAL_OAUTH_ACTIVE);
        $request->session()->push(Helper::SESSION_KEY_APPID,$app_id);
        $request->session()->save();
    }
    public static function isUserAlreadyLoggedIn(Request $request){
        return ($request->session()->has(Helper::SESSION_KEY_LOGGEDIN) 
               && $request->session()->has(Helper::SESSION_KEY_LOGGED_IN_EMAIL));     
    }

    public static function isOauthAuthorizationInProgress(Request $request){
        return ($request->session()->has(Helper::SESSION_KEY_OAUTH) 
                && $request->session()->has(Helper::SESSION_KEY_APPID));
    }

    public static function cleanLoginSession(Request $request){
         $pull_keys = [
             Helper::SESSION_KEY_LOGGEDIN,
             Helper::SESSION_KEY_LOGGED_IN_EMAIL
         ];
         $request->session()->forget($pull_keys);
    }

    public static function cleanOauthSession(Request $request){
         $pull_keys = [
             Helper::SESSION_KEY_OAUTH,
             Helper::SESSION_KEY_APPID
         ];
         $request->session()->forget($pull_keys);
    }

    public static function getPasswordHash($pass){
        $opt = [
            Helper::KEY_COST => Helper::BCRYPT_ROUNDS
        ];
        return password_hash($pass, PASSWORD_BCRYPT, $opt);
    }
    
    public static function verifyPass($pass, $hash){
        return password_verify($pass,$hash);
    }

    public static function getDBValue($table, $wherekey, $wherevalue, $column){
        return DB::table($table)->where($wherekey, $wherevalue)->value($column);
    }

    public static function getSessionKeyValue(Request $request, $key){
        return $request->session()->get($key);
    }

    public static function saveAuthSession($sessionId, $app_id, $auth_code, $auth_status){
            Auth_Session::create([
                Constants::FIELD_SESSION_ID => $sessionId,
                Constants::FIELD_APP_ID => $app_id,
                Constants::FIELD_AUTH_CODE => $auth_code,
                Constants::FIELD_AUTH_STATUS => $auth_status
            ]);
    }
    
    public static function verifyCodeData($appId, $code){
         return DB::Table(Constants::AUTH_SESSION_TABLE)
                ->where(Constants::FIELD_APP_ID,$appId)
                ->where(Constants::FIELD_AUTH_CODE, $code)
                ->where(Constants::FIELD_AUTH_STATUS, Helper::AUTH_CODE_ACTIVE)
                ->exists();

    }

    public static function verifyTokenData($appId, $tokenField, $tokenValue, $timeField, $validityField){
         return DB::Table(Constants::CRED_INFO_TABLE)
                  ->where(Constants::FIELD_APP_ID, $appId)
                  ->where($tokenField, $tokenValue)
                  ->whereRaw((Constants::TIME_FILTER_PART_ONE).$timeField.(Constants::TIME_FILTER_PART_TWO).
                         Constants::COMP_TIME.$validityField)
                  ->exists();
    }

    public static function insertNewTokenRecord($appId,$auth_code, $token, $refreshToken, 
                                                $tokenValidity, $refreshTokenValidity)
    {
        CredentialInfo::create([
            Constants::FIELD_APP_ID => $appId,
            Constants::FIELD_AUTH_CODE => $auth_code,
            Constants::FIELD_ACCESS_TOKEN => $token,
            Constants::FIELD_REFRESH_TOKEN => $refreshToken,
            Constants::FIELD_TOKEN_VALIDITY => $tokenValidity,
            Constants::FIELD_REFRESH_TOKEN_VALIDITY => $refreshTokenValidity
        ]);                                                                    

    }

    public static function refreshExistingTokenRecord($appId, $refreshToken, $newToken)
    {
        DB::Table(Constants::CRED_INFO_TABLE)
                  ->where(Constants::FIELD_APP_ID, $appId)
                  ->where(Constants::FIELD_REFRESH_TOKEN, $refreshToken)
                  ->update([Constants::FIELD_ACCESS_TOKEN => $newToken]);

    }
    
    public static function invalidateAuthSession($appId, $code){
        DB::Table(Constants::AUTH_SESSION_TABLE)
                 ->where(Constants::FIELD_APP_ID, $appId)
                 ->where(Constants::FIELD_AUTH_CODE, $code)
                 ->update([Constants::FIELD_AUTH_STATUS => Helper::AUTH_CODE_INACTIVE]);
    }
    public static function isTimestampValid($timestamp, $validity){
        $currTime = time();
        return currTime <= $timestamp + $validity;
    }
}