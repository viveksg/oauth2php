<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Database\Constants;

class Auth_Session extends Model
{
    protected $fillable = [
        Constants::FIELD_APP_ID,
        Constants::FIELD_SESSION_ID,
        Constants::FIELD_AUTH_CODE,
        Constants::FIELD_AUTH_STATUS
    ];
}
