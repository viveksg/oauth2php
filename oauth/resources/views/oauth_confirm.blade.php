<!-- /resources/views/login.blade.php -->
<!doctype html>
<html>
  <head>
      <title>Login</title>
      <link href="{{asset('css/app.css')}}" rel="stylesheet">
      <script src="{{asset('js/app.js')}}"></script>
      <script src="{{asset('js/bootstrap.js')}}"></script>
  </head>
  <body>
      <div class = "container base_container">
          <div class = "card">
              <h5 class = "card-header">Oauth Lib</h5>
              <div class = "card-body">
                  <h5 class= "card-title">Confirmation Message</h5>
                  <p class = "card-message">Allow {{$app_id}} to access your data?</p>
                  <a href = "/allowed" class = "btn btn-primary">Yes</a>
                  <a href = "/rejected" class = "btn btn-danger">No</a>
              </div>   
          </div>    
         
      </div>
  </body>
</html>