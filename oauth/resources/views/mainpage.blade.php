<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Main Page</title>
        <link href="{{asset('css/mainpage.css')}}" rel="stylesheet">
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/bootstrap.js')}}"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                <div class="top-right links">
                        <a href="{{ url('/logout') }}">Logout</a>
                </div>
         

            <div class="content">
                <div class="title m-b-md">
                    Hello {{$emailId}}
                </div>
            </div>
        </div>
    </body>
</html>
