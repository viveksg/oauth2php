<!-- /resources/views/login.blade.php -->
<!doctype html>
<html>
  <head>
      <title>Login</title>
      <link href="{{asset('css/app.css')}}" rel="stylesheet">
      <script src="{{asset('js/app.js')}}"></script>
      <script src="{{asset('js/bootstrap.js')}}"></script>
  </head>
  <body>
      <div class = "container login_container">
        <form action = "login" id = "login_form" method = "post">
           @csrf
           <div class = "form-group">
              <label for="emailId">Email Id: </label>
              <input type = "email" class = "form-control" id = "emailId" name = "emailId" placeholder = "Email Id"/>
           </div>
           <div class = "form-group">
              <label for="pass">Password: </label>
              <input type = "password" class = "form-control" name = "password" id = "password"/>
           </div>
           <button type = "submit" class = "btn btn-primary">submit</button>
        </form>
      </div>
  </body>
</html>