<?php

namespace Database;

class Constants{
    public const APP_INFO_TABLE = "app_infos";
    public const AUTH_SESSION_TABLE = "auth__sessions";
    public const CRED_INFO_TABLE = "credential_infos";
    public const USER_TABLE = "users";
    public const PASSWORD_RESET_TABLE = "password_resets";

    public const FIELD_USER_NAME = "name";
    public const FIELD_USER_EMAIL = "email";
    public const FIELD_USER_PASSWORD = "password";
    public const FIELD_APP_NAME = "name";
    public const FIELD_REDIRECT_URI = "redirect_uri";
    public const FIELD_APP_ID = "app_id";
    public const FIELD_CLIENT_SECRET = "client_secret";
    public const FIELD_ID = "id";
    public const FIELD_AUTH_CODE = "auth_code";
    public const FIELD_SESSION_ID = "session_id";
    public const FIELD_TIMESTAMP = "TIMESTAMP";
    public const FIELD_ACCESS_TOKEN = "access_token";
    public const FIELD_REFRESH_TOKEN = "refresh_token";
    public const FIELD_AUTH_STATUS = "auth_status";
    public const FIELD_CREATED_AT = "created_at";
    public const FIELD_UPDATED_AT = "updated_at";
    public const FIELD_VALIDITY = "validity";
    public const FIELD_TOKEN_VALIDITY = "token_validity";
    public const FIELD_REFRESH_TOKEN_VALIDITY = "refresh_token_validity";
    public const APP_STATUS = "app_status";
    public const APP_STATUS_ACTIVE = 100;
    public const APP_STATUS_INACTIVE = 101;
    public const AUTH_INITIATED = 1000;
    public const AUTH_SUCCESSFULL = 1001;
    public const AUTH_NOT_SUCCESSFULL = 1002;
    public const CASCADE = "cascade";
    public const INNO_DB = "InnoDB";
    public const COMP_LIKE = "like";
    public const COMP_TIME = " <= ";
    public const TIME_FILTER_PART_ONE = "unix_timestamp() - unix_timestamp(";
    public const TIME_FILTER_PART_TWO = ")";
}