<?php

use Illuminate\Database\Seeder;
use Database\Constants;
use App\Utils\Helper;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Constants::USER_TABLE)->insert([
            Constants::FIELD_USER_NAME => 'testuser',
            Constants::FIELD_USER_EMAIL => Str::random(10).'@mail.com',
            Constants::FIELD_USER_PASSWORD => Helper::getPasswordHash('password'),
        ]);
    }
}
