<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Constants;
class CredInfoUpate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        Schema::table(Constants::CRED_INFO_TABLE, function (Blueprint $table) {
            $table->renameColumn(Constants::FIELD_VALIDITY, Constants::FIELD_TOKEN_VALIDITY);
            $table->integer(Constants::FIELD_REFRESH_TOKEN_VALIDITY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
