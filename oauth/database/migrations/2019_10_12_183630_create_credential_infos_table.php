<?php
include_once('database/constants/dbconstants.php');
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Constants;
class CreateCredentialInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Constants::CRED_INFO_TABLE, function (Blueprint $table) {
            $table->string(Constants::FIELD_APP_ID);
            $table->string(Constants::FIELD_AUTH_CODE);
            $table->longText(Constants::FIELD_ACCESS_TOKEN);
            $table->longText(Constants::FIELD_REFRESH_TOKEN);
            $table->integer(Constants::FIELD_VALIDITY);
            $table->timestamps();
            // add field for clients info like ip, user-agent etc
            $table->primary(array(
                        Constants::FIELD_APP_ID,Constants::FIELD_AUTH_CODE
            ));
            /*$table->foreign(Constants::FIELD_APP_ID)
                  ->references(Constants::FIELD_APP_ID)
                  ->on(Constants::APP_INFO_TABLE)
                  ->onDelete(Constants::CASCADE);*/
            $table->engine = Constants::INNO_DB;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Constants.CRED_INFO_TABLE);
    }
}
