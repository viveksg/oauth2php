<?php
include_once('database/constants/dbconstants.php');
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Constants;
class CreateAppInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Constants::APP_INFO_TABLE, function (Blueprint $table) {
            $table->string(Constants::FIELD_APP_ID);
            $table->string(Constants::FIELD_APP_NAME);
            $table->string(Constants::FIELD_CLIENT_SECRET);
            $table->longText(Constants::FIELD_REDIRECT_URI);
            $table->primary(array(Constants::FIELD_APP_NAME,Constants::FIELD_APP_ID));
            $table->timestamps();
            $table->engine = Constants::INNO_DB;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Constants.APP_INFO_TABLE);
    }
}
