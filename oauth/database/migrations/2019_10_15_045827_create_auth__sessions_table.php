<?php
include_once("database/constants/dbconstants.php");
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Constants;

class CreateAuthSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Constants::AUTH_SESSION_TABLE, function (Blueprint $table) { 
            $table->string(Constants::FIELD_SESSION_ID);
            $table->string(Constants::FIELD_APP_ID);
            $table->string(Constants::FIELD_AUTH_CODE);
            $table->integer(Constants::FIELD_AUTH_STATUS);
            $table->primary(Constants::FIELD_SESSION_ID);
            $table->timestamps();
            $table->engine = Constants::INNO_DB;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Constants::AUTH_SESSION_TABLE);
    }
}
